package com.example.tarea2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GestureDetectorCompat;

import android.graphics.Color;
import android.media.Image;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements  View.OnTouchListener {



    LinearLayout mainlayout;
    EditText inputtext;
    TextView pantalla;
    TextView salir;
    TextView borrar;
    // Global
    GestureDetector gestureDetector;
    GridLayout colors;

    int ObjetoPresionado=0;
    int colorindex=0;
    String[] colorscode = {"#ffffff","#e6e6e6", "#999999", "#4d4d4d", "#000000"};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainlayout = (LinearLayout) findViewById(R.id.main_grid);

        inputtext = (EditText)  findViewById(R.id.inputtext);
        pantalla = (TextView)  findViewById(R.id.pantalla);
        borrar = (TextView)  findViewById(R.id.borrar);
        salir = (TextView)  findViewById(R.id.salir);
        colors =(GridLayout) findViewById(R.id.colors);



        gestureDetector = new GestureDetector(this, new OnSwipeListener() {


            @Override
            public boolean onSwipe(Direction direction) {

            /*
                if (ObjetoPresionado==colors.getId() && (direction == Direction.down || direction == Direction.up))
                {


                }
                */

                if (direction == Direction.up) {
                    //do your stuff
                    Log.d("TAG", "onSwipe: up" );

                    if (ObjetoPresionado==colors.getId())
                    {
                        if (colorindex>0)
                        {
                            Log.d("TAG", "colorindex: "+ colorindex);
                            colorindex--;}

                    }

                }

                if (direction == Direction.down) {
                    //do your stuff
                    Log.d("TAG", "onSwipe: down");


                    if (ObjetoPresionado==colors.getId())
                    {
                        if (colorindex<4)

                        {

                        Log.d("TAG", "colorindex: "+ colorindex);
                            colorindex++;
                        }

                    }

                }





                if (direction == Direction.right) {
                    //do your stuff
                    Log.d("TAG", "onSwipe: Right");




                    if (ObjetoPresionado==salir.getId())
                    {
                        finish();
                        System.exit(0);

                    }

                }

                if (direction == Direction.left) {
                    //do your stuff
                    Log.d("TAG", "onSwipe: Left");

                    if (ObjetoPresionado==borrar.getId())
                    {
                        inputtext.setText("");
                        pantalla.setText("");

                    }

                }
                return true;
            }


        });

        salir.setOnTouchListener(this);
        borrar.setOnTouchListener(this);
        colors.setOnTouchListener(this);

       //mainlayout.setBackgroundColor(Color.parseColor("#fcba03"));
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        //Log.d("TAG",  String.valueOf(v.getId()));
        ObjetoPresionado= v.getId();

        gestureDetector.onTouchEvent(event);

        if (ObjetoPresionado == colors.getId())
        {
            try {
                Log.d("TAG", "colorindex: "+ colorindex);
                mainlayout.setBackgroundColor(Color.parseColor(colorscode[colorindex]));

            } catch (Exception e) {


                Log.i("TAG", "Error " + e.getMessage());
            }

        }
        return true;
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_VOLUME_UP)){
            //Do something
            try {

                    if (!TextUtils.isEmpty(inputtext.getText().toString().trim())) {

                        double cantidad = Double.parseDouble(inputtext.getText().toString());
                        //double resultado = 0;
                        String resultado="";


                        //100, 50, 20 tiene y moneda de 5 y 10 centavos


                        //pantalla.setText(String.format("%.2f", resultado));

                        if (cantidad>=100)  {resultado+="100->" + String.valueOf( (int) (cantidad/100));}

                        if (cantidad>=50) {resultado+=" | 50->" + String.valueOf( (int) (cantidad/50));}

                        if (cantidad>=20) { resultado+=" | 20->" + String.valueOf( (int) (cantidad/20));}

                        if (cantidad>=.05) {resultado+=" | 5c->" + String.valueOf( (int) (cantidad/0.05));}
                        if (cantidad>=0.1) {resultado+=" | 10c->" + String.valueOf( (int) (cantidad/0.1));}

                        pantalla.setText(resultado);

                        Log.i("TAG", "resultado " + resultado);
                    }



            } catch (Exception e) {


                Log.i("TAG", "Error " + e.getMessage());
            }

        }
        return true;
    }



    /*    @Override
        public boolean onTouch(View v, MotionEvent event) {

            int x = (int) event.getX();
            int y = (int) event.getY();
            Log.i("TAG", "124");
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                   // Log.i("TAG", "touched down");
                    break;
                case MotionEvent.ACTION_MOVE:
                    Log.i("TAG", "moving: (" + x + ", " + y + ")");
                    break;
                case MotionEvent.ACTION_UP:
                  //  Log.i("TAG", "touched up");
                    break;
            }

            return true;
        }
*/
/*
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        float initialX=0, initialY=0;
        int action = event.getActionMasked();

        switch (action) {

            case MotionEvent.ACTION_DOWN:
                initialX = event.getX();
                initialY = event.getY();


                 Log.d("TAG", "Action was DOWN");
                break;

            case MotionEvent.ACTION_MOVE:

                Log.d("TAG", "Action was MOVE");
                break;

            case MotionEvent.ACTION_UP:
                float finalX = event.getX();
                float finalY = event.getY();


                Log.d("TAG", "Action was UP");

                if (initialX < finalX) {
                     Log.d("TAG", "Left to Right swipe performed");
                }

                if (initialX > finalX) {
                     Log.d("TAG", "Right to Left swipe performed");
                }

                if (initialY < finalY) {
                     Log.d("TAG", "Up to Down swipe performed");
                }

                if (initialY > finalY) {
                     Log.d("TAG", "Down to Up swipe performed");
                }

                break;

            case MotionEvent.ACTION_CANCEL:
                Log.d("TAG","Action was CANCEL");
                break;

            case MotionEvent.ACTION_OUTSIDE:
                 Log.d("TAG", "Movement occurred outside bounds of current screen element");
                break;
        }

        return super.onTouchEvent(event);
    }
*/
}
